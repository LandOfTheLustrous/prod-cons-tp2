package ca.qc.claurendeau.storage;

public class Element {
	private int data;
	private Element prochain;

	public void setData(int data) {
		this.data = data;
	}

	public int getData() {
		return this.data;
	}

	public Element getProchain() {
		return prochain;
	}

	public void setProchain(Element prochain) {
		this.prochain = prochain;
	}

	@Override
	public String toString() {
		return String.valueOf(data);
	}
	
	
	
	
}
