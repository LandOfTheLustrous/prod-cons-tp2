package ca.qc.claurendeau.storage;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.qc.claurendeau.exception.BufferCapacityException;
import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer {
	private Element tete;
	private Element queue;
	private Integer capacite;
	private Integer nombrePresent = 0;

	public Buffer(int capacite) throws BufferCapacityException {
		if (isCapacityTooSmall(capacite)) throw new BufferCapacityException("No capacity or negative number");
		this.capacite = capacite;
	}

	private boolean isCapacityTooSmall(int capacite) {
		return capacite < 1;
	}

	public int capacity() {
		return capacite;
	}
	
	public int getCurrentLoad() {
		return nombrePresent;
	}

	public boolean isEmpty() {
		return nombrePresent == 0 ? true : false;
	}

	public boolean isFull() {
		return nombrePresent == capacite;
	}

	public synchronized void addElement(Element element) throws BufferFullException {
		if (isFull()) throw new BufferFullException("Buffer is full");
		
		if (isEmpty()) {
			tete = element;
		} else {
			queue.setProchain(element);
		}
		queue = element;
		nombrePresent++;
	}

	public synchronized Element removeElement() throws BufferEmptyException {
		if (isEmpty()) throw new BufferEmptyException("Buffer is empty");
		if (isSingleton()) queue = null;
		
		Element premier = tete;
		tete = tete.getProchain();
		nombrePresent--;
		
		return premier;
	}

	private boolean isSingleton() {
		return nombrePresent == 1;
	}

	public String toString() {
		return toList().toString();
	}

	public Element getTete() {
		return tete;
	}
	
	private List<Integer> toList() {
		return Stream.iterate(this.tete, Element::getProchain).limit(nombrePresent).map(Element::getData).collect(Collectors.toList());
	}

}
