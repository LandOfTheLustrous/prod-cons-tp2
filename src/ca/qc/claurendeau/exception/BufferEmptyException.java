package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public BufferEmptyException(String exception) {
		super(exception);
	}
}
