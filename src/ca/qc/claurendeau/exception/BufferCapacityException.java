package ca.qc.claurendeau.exception;

public class BufferCapacityException extends Exception {

	private static final long serialVersionUID = 1L;

	public BufferCapacityException(String exception) {
		super(exception);
	}
	
	
}
