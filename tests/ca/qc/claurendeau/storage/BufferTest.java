package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Test;

import ca.qc.claurendeau.exception.BufferCapacityException;
import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {

    @Test
    public void testCapacite() throws BufferCapacityException {
    	int capaciteVoulu = 20;
    	Buffer buffer = new Buffer(capaciteVoulu);
        assertTrue(buffer.capacity() == capaciteVoulu);
    }
    
    @Test(expected = BufferCapacityException.class)
    public void testCapaciteNegative() throws BufferCapacityException {
    	int capaciteVoulu = -1;
    	new Buffer(capaciteVoulu);
    }
    
    @Test
    public void testAddToBuffer() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(5);
    	buffer.addElement(new Element());
    	assertEquals(1, buffer.getCurrentLoad());
    }
    
    @Test
    public void testAddValeurElement() throws BufferCapacityException {
    	Element element = new Element();
    	element.setData(12);
    	assertEquals(12, element.getData());
    }
    
    @Test
    public void testBufferEstVide() throws BufferCapacityException {
    	Buffer buffer = new Buffer(5);
    	assertTrue(buffer.isEmpty());
    }
    
    @Test
    public void testBufferEstPasVide() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(5);
    	buffer.addElement(new Element());
    	assertFalse(buffer.isEmpty());
    }
    
    @Test
    public void testAddMultiple() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(5);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	buffer.addElement(element1);
    	buffer.addElement(element2);

    	assertEquals(1, buffer.getTete().getData());
    	assertEquals(2, buffer.getTete().getProchain().getData());
    	assertEquals(2, buffer.getCurrentLoad());
    }
    
    @Test(expected = BufferFullException.class)
    public void testBufferFullWhenAdding() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(2);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	
    	Element element3 = new Element();
    	element3.setData(3);
    	buffer.addElement(element3);
    }
    
    @Test
    public void testBufferIsFull() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(2);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	
    	buffer.addElement(element1);
    	assertFalse(buffer.isFull());
    	
    	buffer.addElement(element2);
    	assertTrue(buffer.isFull());

    }
    
    @Test
    public void testToStringAvecData() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(2);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	
    	assertEquals("[1, 2]", buffer.toString());
    }
    
    @Test
    public void testToStringVide() throws BufferCapacityException, BufferFullException {
    	Buffer buffer = new Buffer(5);
    	assertEquals("[]", buffer.toString());
    }
    
    @Test
    public void testRemoveUnElement() throws BufferCapacityException, BufferFullException, BufferEmptyException {
    	Buffer buffer = new Buffer(2);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	
    	assertEquals(element1.toString(), buffer.removeElement().toString());
    }

    
    @Test
    public void testRemoveAll() throws BufferCapacityException, BufferFullException, BufferEmptyException {
    	Buffer buffer = new Buffer(10);
    	Element element1 = new Element();
    	element1.setData(1);
    	Element element2 = new Element();
    	element2.setData(2);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	
    	assertEquals(2, buffer.getCurrentLoad());
    	buffer.removeElement();
    	buffer.removeElement();
    	assertEquals(0, buffer.getCurrentLoad());
    	assertEquals(null, buffer.getTete());
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testRemoveIfEmpty() throws BufferCapacityException, BufferFullException, BufferEmptyException {
    	Buffer buffer = new Buffer(5);
    	buffer.removeElement();
    	
    	assertEquals(0, buffer.getCurrentLoad());
    }
    
    @Test
    public void testAddRemoveToStringMultiple() throws BufferCapacityException, BufferFullException, BufferEmptyException {
    	Buffer buffer = new Buffer(5);
    	Element element1 = new Element();
    	Element element2 = new Element();
    	Element element3 = new Element();
    	Element element4 = new Element();
    	Element element5 = new Element();
    	element1.setData(1);
    	element2.setData(2);
    	element3.setData(3);
    	element4.setData(4);
    	element5.setData(5); 	
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	buffer.addElement(element3);
    	buffer.addElement(element4);
    	buffer.addElement(element5);
    	
    	assertEquals(5, buffer.getCurrentLoad());
    	assertTrue(buffer.isFull());
    	assertEquals("[1, 2, 3, 4, 5]", buffer.toString());
    	
    	buffer.removeElement();
    	buffer.removeElement();
    	
    	assertEquals(3, buffer.getCurrentLoad());
    	assertFalse(buffer.isFull());
    	assertEquals("[3, 4, 5]", buffer.toString());
    	
    	buffer.removeElement();
    	buffer.removeElement();
    	
    	assertEquals(1, buffer.getCurrentLoad());
    	assertEquals("[5]", buffer.toString());
    	
    	buffer.removeElement();
    	
    	assertEquals(0, buffer.getCurrentLoad());
    	assertTrue(buffer.isEmpty());
    	assertEquals("[]", buffer.toString()); 	
    }
    
}


